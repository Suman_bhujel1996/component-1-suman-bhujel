﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Graphical_Programming_Application
{
    class Triangle : Shape
    {
        PointF p1, p2, p3;
        public override double calcArea()
        {
            throw new NotImplementedException();
        }

        public override double calcPerimeter()
        {
            throw new NotImplementedException();
        }

        public void setPoint(int point1, int point2, int point3, int point4, int point5, int point6)
        {
            this.p1 = new PointF(point1, point2);
            this.p2 = new PointF(point3, point4);
            this.p3 = new PointF(point5, point6);
        }
        public override void draw(Graphics g, Color c, int thickness)
        {
            Pen p = new Pen(c, thickness);
            SolidBrush b = new SolidBrush(colour);
            PointF[] curvePoints =
            {
                p1,p2,p3
            };
            g.DrawPolygon(p, curvePoints);
            g.FillPolygon(b, curvePoints);
        }
    }
}
