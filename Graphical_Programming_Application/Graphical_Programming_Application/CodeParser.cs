﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Graphical_Programming_Application
{
    class CodeParser
    {
        Boolean error = false;
        string error_cmm;
        public CodeParser() { }
        public string Parser(string comma, string code,int i)
        {

            string comm = comma.ToLower();
            string cods = code.ToLower();

            switch (comm)
            {
                case "run":
                    try
                    {
                        String code_line = cods;
                        char[] code_delimiters = new char[] { ' ' };
                        String[] words = code_line.Split(code_delimiters, StringSplitOptions.RemoveEmptyEntries);
                        if (words[0] == "draw")
                        {
                            if (words[1] == "circle")
                            {
                                if (!(words.Length == 3))
                                {
                                    error = true;
                                    error_cmm = "the error in on line " + i + " please type correct code for 'draw circle'";
                                    return error_cmm;
                                }
                                else
                                {

                                    string shape = "circle " + words[2];
                                    return shape;
                                }
                            }
                            else if (words[1] == "square")
                            {
                                if (!(words.Length == 3))
                                {
                                    error = true;
                                    error_cmm = "the error in on line " + i + " pls type correct code for 'draw square'";

                                }
                                else
                                {
                                    string shape = "square " + words[2];
                                    return shape;

                                }
                            }

                            else if (words[1] == "rectangle")
                            {
                                if (!(words.Length == 4))
                                {
                                    error = true;
                                    error_cmm = "the error in on line " + i + " pls type correct code for 'draw rectangle'";

                                }
                                else
                                {
                                    string shape = "rectangle " + words[2] + " " + words[3];
                                    return shape;

                                }
                            }
                            else if (words[1] == "triangle")
                            {
                                if (!(words.Length == 8))
                                {
                                    error = true;
                                    error_cmm = "the error in on line " + i + " pls type correct code for 'draw triangle'";

                                }
                                else 
                                {
                                    string shape = "triangle " + words[2] + " " + words[3] + " " + words[4] + " " + words[5] + " " + words[6] + " " + words[7];
                                    return shape;
                                }
                            }
                            else
                            {
                                return "false";
                            }
                        }
                        else if (words[0] == "move")
                        {
                            if (!(words.Length == 3))
                            {
                                error = true;
                                error_cmm = "the error in on line " + i + " pls type correct code for 'draw square'";
                                return error_cmm;
                            }
                            else
                            {
                                return ("move " + words[1] + " " + words[2]);
                            }
                        }
                        else if (words[0] == "color")
                        {
                            if (!(words.Length == 3))
                            {
                                error = true;
                                error_cmm = "the error in on line " + i + " pls type correct code for 'move'";
                                return error_cmm;
                            }
                            else 
                            {

                                return code_line;
                           
                                

                            }
                        }
                        else if (words[0] == "fill")
                        {
                            if (!(words.Length == 2))
                            {
                                error = true;
                                error_cmm = "the error in on line " + i + " pls type correct code for 'move'";
                                return error_cmm;
                            }

                            else
                            {
                                return code_line;

                            }
                        }
                        else
                        {
                            return "false";

                        }


                    }
                    catch (IndexOutOfRangeException ex)
                    {
                        return "false";

                    }
                    break;
                case "clear":
                    return "clear";
                case "reset":
                    return "reset";
                case "help":
                    return "help";
                default:
                    return "False";


            }
            return null;
        }
    }
}
